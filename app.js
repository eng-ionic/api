const express = require('express');
const app = express();
const port = 3000;
var bodyParser = require('body-parser');
var cors = require('cors')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())
// parse application/json
app.use(bodyParser.json())

movies = [];

app.get('/movies', (req, res) => {
    console.log(movies);
    res.json(movies);
})

app.get('/movies/:id', (req, res) => {
    console.log(req.params.id);
    res.json(movies.find(m => m.id == req.params.id));
})

app.post('/movies', (req, res) => {
    console.log(req.body);
    movies.push({
        ...req.body,
        id: movies.length + 1
    });
    res.json(movies);
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});